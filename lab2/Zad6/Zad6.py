word = input("Enter a string:")
rev = word[::-1]
if word == rev:
    print("This string is a palindrome")
else:
    print("This string is not a palindrome")