import random

chars = "qwertzuiopšđasdfghjklčćžyxcvbnm1234567890'+,.-Q!#$%&/()=?*~˘°QWERTZUIOPŠĐASDFGHJKLČĆŽYXCVBNM;:_"
password = ""
for c in range(12):
    password += random.choice(chars)
print(password)